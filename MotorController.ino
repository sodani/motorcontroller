
// libraries
//#include <Wire.h>

// debugging
#define SERIALDEBUG 6

// motor definitions
#define A_PWM 3
#define A_IN2 4
#define A_IN1 5
#define STBY 6
#define B_IN1 7
#define B_IN2 8
#define B_PWM 9

#define ACS712PIN A0 // for monitoring the current

// constants won't change. They're used here to set pin numbers:
const int buttonPin10 = 10;     // the number of the pushbutton pin
const int buttonPin11 = 11;     // the number of the pushbutton pin
const int ledPin =  13;      // the number of the LED pin

int buttonState10 = 0;         // variable for reading the pushbutton status
int buttonState11 = 0;         // variable for reading the pushbutton status

// variables needed
int tiller_direction = 0;
int tillerCurrent = 0;
unsigned long pulseStartMillis;
unsigned long pulseCurrentMillis;
unsigned long overloadStartMillis;
unsigned long overloadCurrentMillis;

// variables for overload
#define OVERLOADCURRENT 6000 // milliAmps. Overload current is around 6120 for my motor
long refVoltage = 2490; //mV for ACS712
int overloadDirection = 0;
bool overload = false;

void tillerPush() {
  // turn LED on:
  //digitalWrite(ledPin, HIGH);

  // push tiller
  digitalWrite(A_IN1, LOW);
  digitalWrite(A_IN2, HIGH);
  digitalWrite(B_IN1, LOW);
  digitalWrite(B_IN2, HIGH);
  digitalWrite(A_PWM, HIGH);
  digitalWrite(B_PWM, HIGH);

}

void tillerPull(){
  // turn LED on:
  //digitalWrite(ledPin, HIGH);

  // pull tiller
  digitalWrite(A_IN1, HIGH);
  digitalWrite(A_IN2, LOW);
  digitalWrite(B_IN1, HIGH);
  digitalWrite(B_IN2, LOW);
  digitalWrite(A_PWM, HIGH);
  digitalWrite(B_PWM, HIGH);

}

bool tillerOverload(int commandDirection) {
  const int sensitivity = 100;//change this to 100 for ACS712PIN-20A or to 66 for ACS712PIN-30A

  // if there was an overload the the direction is the same
  if (overload == true && overloadDirection == commandDirection) {
    return true;
  }

  // if there was a pull overload command direction is push
  if (overload == true && overloadDirection == 1 && commandDirection == -1) {
    overload = false;
    return false;
  }

  // there was a push overload, but extra current is needed for the initial pull
  if (overloadDirection == -1 && commandDirection == 1){
    overloadCurrentMillis = millis();
    if(overload == true) {
      // start overload timer
      overloadStartMillis = millis();
      overload = false;
      #if SERIALDEBUG==6
        Serial.println("overload start timer");
      #endif
      return false;
    } else if (overloadCurrentMillis - overloadStartMillis < 5000){
      #if SERIALDEBUG==6
        Serial.println("within overload timer period");
      #endif
      return false; // 5 second timer to recover
    }
  }

  // if there was no overload, then start measuring the current
  int v = 0;
  //read some samples to stabilize value
  for (int i = 0; i < 10; i++) {
    v = v + analogRead(ACS712PIN);
    delay(2);
  }
  v /= 10;
  v = 4.88*v; // this will give voltage in mV
  #if SERIALDEBUG==4
    Serial.print("v: ");
    Serial.println(v);
  #endif

  // this is giving 3.9 Amps for my motor in the TP32
  tillerCurrent = 1000*(v - refVoltage)/sensitivity; // to mA. 

  #if SERIALDEBUG==5
    Serial.print("current: ");
    Serial.println(tillerCurrent);
  #endif
  if (abs(tillerCurrent) > OVERLOADCURRENT) {
    // overloaded
    overload = true;
    overloadDirection = commandDirection;

    #if SERIALDEBUG==6
      Serial.print("Overload Current: ");
      Serial.println(tillerCurrent);
      Serial.print("tiller direction: ");
      Serial.println(commandDirection);
      Serial.print("Overload Direction: ");
      Serial.println(overloadDirection);
    #endif
    
    return true;
  }
  else {
    // not overloaded
    overloadDirection = 0;
    #if SERIALDEBUG==5
      Serial.print("Not Overload");
    #endif
    return false;
  }
}

void setup() {
  #if SERIALDEBUG
    Serial.begin(115200);
    Serial.println("Initializing...");
  #endif

  // initialize the LED pin as an output:
  pinMode(ledPin, OUTPUT);

  // initialize the pushbutton pin as an input:
  pinMode(buttonPin10, INPUT);
  pinMode(buttonPin11, INPUT);

  // initializing pins for motor
  pinMode(STBY, OUTPUT);
  pinMode(A_PWM, OUTPUT);
  pinMode(A_IN1, OUTPUT);
  pinMode(A_IN2, OUTPUT);
  pinMode(B_PWM, OUTPUT);
  pinMode(B_IN1, OUTPUT);
  pinMode(B_IN2, OUTPUT);
  digitalWrite(STBY, HIGH);

  // put motor on standby
  digitalWrite(A_PWM, LOW);
  digitalWrite(B_PWM, LOW);
}

// NEXT
// if keep pushing until overloaded, then initial pull needs extra current and shouldn't be subject to overload check
void loop() {
  // read the state of the pushbutton value:
  // the issue is with pin 1
  buttonState10 = digitalRead(buttonPin10);
  buttonState11 = digitalRead(buttonPin11);
  tillerOverload(tiller_direction);

  // stop motor at the end of pulse
  pulseCurrentMillis = millis();
  if((pulseCurrentMillis - pulseStartMillis) > 100) {
    // stop motor
    digitalWrite(A_PWM, LOW);
    digitalWrite(B_PWM, LOW);
  }

  // check if the pushbutton is pressed. If it is, the buttonState is HIGH:
  if (buttonState10 == HIGH) {
    tiller_direction = 1;

    // check overload
    if(tillerOverload(tiller_direction) == false){
      pulseStartMillis = millis();
      tillerPull();
    };
    //Serial.println(tiller_direction);
  } 
  
  if (buttonState11 == HIGH){
    tiller_direction = -1;

    // check overload
    if(tillerOverload(tiller_direction) == false){
      pulseStartMillis = millis();
      tillerPush();
    };

    //tillerOverload(tiller_direction);
  }
}
